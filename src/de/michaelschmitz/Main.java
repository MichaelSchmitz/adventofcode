package de.michaelschmitz;

import de.michaelschmitz.days.Day;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {
    static int[] days = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};

    private static List<String> readInput(String day, boolean sample) {
        try (var lines = Files.lines(Paths.get("%sInputs/day%s.src".formatted(sample ? "sample" : "puzzle", day)))) {
            return lines.toList();
        } catch (IOException e) {
            return List.of();
        }
    }

    private static void executePart(String day, int part, Day daySolver, List<String> input) {

        var startTime = System.nanoTime();
        var result = part == 1 ? daySolver.executePartOne(input) : daySolver.executePartTwo(input);
        var endTime = System.nanoTime();
        System.out.printf("Day %s, Part %d: %d  (took %dms)%n", day, part, result, (endTime - startTime) / 1000000);
    }

    public static void main(String[] args) {
        var sample = false;
        for (var day : days) {
            var prefixedDay = String.valueOf(day < 10 ? "0" + day : day);
            var input = readInput(prefixedDay, sample);
            if (input.size() > 0) {
                var daySolver = DayFactory.createDay(day);
                executePart(prefixedDay, 1, daySolver, input);
                executePart(prefixedDay, 2, daySolver, input);
            }
        }
    }
}
