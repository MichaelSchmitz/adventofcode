package de.michaelschmitz.days;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Day19 implements Day {
    private final static boolean DEBUG_PRINT = true;
    private final static int SQUARE_SIZE = 100;

    private static class Position {
        public int x;
        public int y;

        public Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Position position = (Position) o;
            return x == position.x && y == position.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }

    private void printBeam(List<Position> positions, int fieldSize) {
        if (DEBUG_PRINT) {
            System.out.println();
            for (int y = 0; y < fieldSize; y++) {
                for (int x = 0; x < fieldSize; x++) {
                    System.out.print(positions.contains(new Position(x, y)) ? '#' : '_');
                }
                System.out.println();
            }
        }
    }

    @Override
    public long executePartOne(List<String> input) {
        var positions = runCode(input.get(0), 50);
        printBeam(positions, 50);
        return positions.size();
    }

    @Override
    public long executePartTwo(List<String> input) {
        var positions = runCode(input.get(0), 300);
        var lowerMaxY = positions.stream().max(Comparator.comparingInt(p -> p.y)).orElse(new Position(0, 0)).y;
        var upperMaxX = positions.stream().max(Comparator.comparingInt(p -> p.x)).orElse(new Position(0, 0)).x;
        var lowerDiagonal = positions.stream().filter(p -> p.y == lowerMaxY).min(Comparator.comparingInt(Position::getX)).orElse(new Position(0, 0));
        var upperDiagonal = positions.stream().filter(p -> p.x == upperMaxX).min(Comparator.comparingInt(Position::getY)).orElse(new Position(0, 0));
        if (lowerDiagonal.y == 0 || upperDiagonal.x == 0)
            return -1;
        var lowerGradient = lowerDiagonal.y / (double) lowerDiagonal.x;
        var upperGradient = upperDiagonal.y / (double) upperDiagonal.x;
        boolean doesNotFit = true;
        int leftX = 0;
        long result = -1;
        while (doesNotFit) {
            var lowerLeft = new Position(leftX, (int) (lowerGradient * leftX));
            var upperRight = new Position(lowerLeft.x + SQUARE_SIZE - 1, lowerLeft.y - SQUARE_SIZE + 1);
            if (upperRight.y >= (int) (upperGradient * upperRight.x)) {
                doesNotFit = false;
                result = lowerLeft.x * 10000L + upperRight.y;
            }
            leftX++;
        }
        return result;
    }

    private List<Position> runCode(String input, int fieldSize) {
        var positions = new ArrayList<Position>();
        try {

            LinkedBlockingQueue<Long> queue1 = new LinkedBlockingQueue<>();
            LinkedBlockingQueue<Long> queue2 = new LinkedBlockingQueue<>();
            for (int x = 0; x < fieldSize; x++) {
                var stationaryFoundY = false;
                var noStationaryFoundY = false;
                for (int y = 0; y < fieldSize; y++) {
                    if (!noStationaryFoundY) {
                        queue1.put((long) x);
                        queue1.put((long) y);
                        new Intcode(queue1, queue2, input).run();
                        if (queue2.take() == 1) {
                            positions.add(new Position(x, y));
                            stationaryFoundY = true;
                        } else if (stationaryFoundY) {
                            noStationaryFoundY = true;
                        }
                    }
                }
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return positions;
    }

    private static class Intcode {
        long relativeBase = 0;
        BlockingQueue<Long> in;
        BlockingQueue<Long> out;
        String input;
        long result;

        Intcode(BlockingQueue<Long> in, BlockingQueue<Long> out, String input) {
            this.in = in;
            this.out = out;
            this.input = input;
        }

        public void run() {
            try {
                result = doProcessing();
            } catch (InterruptedException ignored) {
            }
        }

        private Instruction getInstruction(long inst, Map<Long, Long> op, long ip, int size) {
            long val1 = getParam(op, ip, getMode(inst, 1), 1, inst % 10 == 3);
            long val2 = -1;
            long val3 = -1;
            if (size >= 2)
                val2 = getParam(op, ip, getMode(inst, 2), 2, inst % 10 == 3);
            if (size >= 3)
                val3 = getParam(op, ip, getMode(inst, 3), 3, true);
            return new Instruction(val1, val2, val3);
        }

        private long getParam(Map<Long, Long> op, long ip, int mode, int pos, boolean rel) {
            Long tmp = op.getOrDefault(ip + pos, 0L);
            switch (mode) {
                case 0:
                    return rel ? tmp : op.getOrDefault(tmp, 0L);
                case 1:
                    return tmp;
                case 2:
                    return rel ? relativeBase + tmp : op.getOrDefault(relativeBase + tmp, 0L);
                default:
                    System.err.println("Wrong mode");
                    return -1;
            }
        }

        private int getMode(long inst, int pos) {
            return (int) (inst / Math.pow(10, pos + 1) % 10);
        }

        long doProcessing() throws InterruptedException {
            String[] opTmp = input.split(",");
            Map<Long, Long> op = new HashMap<>();
            for (long i = 0; i < opTmp.length; i++) {
                op.put(i, Long.parseLong(opTmp[(int) i]));
            }
            for (long i = 0; i < op.size(); ) {
                long currentCode = op.getOrDefault(i, 0L);
                switch ((int) (currentCode % 100)) {
                    case 1 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 + inst.val2);
                        i += 4;
                    }
                    case 2 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 * inst.val2);
                        i += 4;
                    }
                    case 3 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 1);
                        op.put(inst.val1, in.take());
                        i += 2;
                    }
                    case 4 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 1);
                        out.put(inst.val1);
                        i += 2;
                    }
                    case 5 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 2);
                        if (inst.val1 != 0L) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                    }
                    case 6 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 2);
                        if (inst.val1 == 0L) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                    }
                    case 7 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 < inst.val2 ? 1L : 0L);
                        i += 4;
                    }
                    case 8 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 == inst.val2 ? 1L : 0L);
                        i += 4;
                    }
                    case 9 -> {
                        relativeBase += getInstruction(currentCode, op, i, 1).val1;
                        i += 2;
                    }
                    case 99 -> {
                        return op.getOrDefault(0L, 0L);
                    }
                    default -> {
                        System.err.println("False opcode");
                        return -1L;
                    }
                }
            }
            return -1L;
        }

        private static class Instruction {
            long val1;
            long val2;
            long target;

            Instruction(long val1, long val2, long target) {
                this.val1 = val1;
                this.val2 = val2;
                this.target = target;
            }
        }
    }
}
