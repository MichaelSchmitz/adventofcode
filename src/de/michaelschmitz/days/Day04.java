package de.michaelschmitz.days;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Day04 implements Day {
    private static int checkNumber(int input, boolean groupCriteriaEnabled) {
        String number = String.valueOf(input);
        if (number.chars().distinct().count() < 6) {
            AtomicInteger prevPos = new AtomicInteger(-1);
            AtomicBoolean result = new AtomicBoolean(true);
            List<Integer> groupCounter = new LinkedList<>();
            number.chars().forEach(pos -> {
                if (pos == prevPos.get()) {
                    groupCounter.set(groupCounter.size() - 1, groupCounter.get(groupCounter.size() - 1) + 1);
                } else {
                    groupCounter.add(1);
                }
                result.set(result.get() && pos >= prevPos.get());
                prevPos.set(pos);
            });
            var hasGroup = groupCounter.stream().anyMatch(count -> groupCriteriaEnabled ? count == 2 : count >= 2);
            return result.get() && hasGroup ? 1 : 0;
        }
        return 0;
    }

    @Override
    public long executePartOne(List<String> input) {
        int start = Integer.parseInt(input.get(0).split("-")[0]);
        int end = Integer.parseInt(input.get(0).split("-")[1]);
        int current = start;
        int count = 0;
        while (current < end) {
            count += checkNumber(current++, false);
        }
        return count;
    }

    @Override
    public long executePartTwo(List<String> input) {
        int start = Integer.parseInt(input.get(0).split("-")[0]);
        int end = Integer.parseInt(input.get(0).split("-")[1]);
        int current = start;
        int count = 0;
        while (current < end) {
            count += checkNumber(current++, true);
        }
        return count;
    }
}
