package de.michaelschmitz.days;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Day09 implements Day {
    private static long runCode(Long input, String code) {
        try {
            List<LinkedBlockingQueue<Long>> streams = Stream.generate((Supplier<LinkedBlockingQueue<Long>>) LinkedBlockingQueue::new)
                    .limit(2).toList();
            streams.get(0).put(input);
            BoostThread thread = new BoostThread(streams.get(0), streams.get(1), code);
            thread.start();
            thread.join();
            return streams.get(1).stream().toList().get(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public long executePartOne(List<String> input) {
        return runCode(1L, input.get(0));
    }

    @Override
    public long executePartTwo(List<String> input) {
        return runCode(2L, input.get(0));
    }

    private static class BoostThread extends Thread {
        BlockingQueue<Long> in;
        BlockingQueue<Long> out;
        long relativeBase = 0;
        String input;

        BoostThread(BlockingQueue<Long> in, BlockingQueue<Long> out, String input) {
            this.in = in;
            this.out = out;
            this.input = input;
        }

        public void run() {
            try {
                doProcessing(input);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private Instruction getInstruction(long inst, Map<Long, Long> op, long ip, int size) {
            long val1 = getParam(op, ip, getMode(inst, 1), 1, inst % 10 == 3);
            long val2 = -1;
            long val3 = -1;
            if (size >= 2)
                val2 = getParam(op, ip, getMode(inst, 2), 2, inst % 10 == 3);
            if (size >= 3)
                val3 = getParam(op, ip, getMode(inst, 3), 3, true);
            return new Instruction(val1, val2, val3);
        }

        private long getParam(Map<Long, Long> op, long ip, int mode, int pos, boolean rel) {
            Long tmp = op.getOrDefault(ip + pos, 0L);
            switch (mode) {
                case 0:
                    return rel ? tmp : op.getOrDefault(tmp, 0L);
                case 1:
                    return tmp;
                case 2:
                    return rel ? relativeBase + tmp : op.getOrDefault(relativeBase + tmp, 0L);
                default:
                    System.err.println("Wrong mode");
                    return -1;
            }
        }

        private int getMode(long inst, int pos) {
            return (int) (inst / Math.pow(10, pos + 1) % 10);
        }

        void doProcessing(String input) throws InterruptedException {
            String[] opTmp = input.split(",");
            Map<Long, Long> op = new HashMap<>();
            for (long i = 0; i < opTmp.length; i++) {
                op.put(i, Long.parseLong(opTmp[(int) i]));
            }
            for (long i = 0; i < op.size(); ) {
                long currentCode = op.getOrDefault(i, 0L);
                switch ((int) (currentCode % 100)) {
                    case 1 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 + inst.val2);
                        i += 4;
                    }
                    case 2 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 * inst.val2);
                        i += 4;
                    }
                    case 3 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 1);
                        op.put(inst.val1, in.take());
                        i += 2;
                    }
                    case 4 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 1);
                        out.put(inst.val1);
                        i += 2;
                    }
                    case 5 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 2);
                        if (inst.val1 != 0L) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                    }
                    case 6 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 2);
                        if (inst.val1 == 0L) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                    }
                    case 7 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 < inst.val2 ? 1L : 0L);
                        i += 4;
                    }
                    case 8 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 == inst.val2 ? 1L : 0L);
                        i += 4;
                    }
                    case 9 -> {
                        relativeBase += getInstruction(currentCode, op, i, 1).val1;
                        i += 2;
                    }
                    case 99 -> {
                        op.getOrDefault(0L, 0L);
                        return;
                    }
                    default -> {
                        System.err.println("False opcode");
                        return;
                    }
                }
            }
        }

        private static class Instruction {
            long val1;
            long val2;
            long target;

            Instruction(long val1, long val2, long target) {
                this.val1 = val1;
                this.val2 = val2;
                this.target = target;
            }
        }
    }
}
