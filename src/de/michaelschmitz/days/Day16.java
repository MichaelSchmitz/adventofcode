package de.michaelschmitz.days;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Day16 implements Day {
    private String doPhase2(String inp) {
        StringBuilder b = new StringBuilder();
        char[] chars = inp.toCharArray();

        int acc = 0;
        for (int i = 0; i < chars.length; i++) {
            int n = chars[chars.length - i - 1] - 48;
            acc += n;
            b.append(acc % 10);
        }
        b.reverse();
        return b.toString();

    }

    private long getOffset(String input) {
        return Long.parseLong(input.substring(0, 7));
    }

    private List<Integer> parseInput(String input) {
        List<Integer> result = new LinkedList<>();
        Arrays.stream(input.split("(?!^)")).map(Integer::parseInt).forEach(result::add);
        return result;
    }

    private String parseInputTimes10000(String input) {
        return String.valueOf(input).repeat(10000);
    }

    private List<Integer> doPhase(List<Integer> input) {
        List<Integer> result = new LinkedList<>();
        for (int i = 0; i <= input.size(); i++) {
            int x = input.subList(i, input.size()).stream().mapToInt(j -> j).sum() % 10;
            result.add(x);
        }
        return result;
    }

    @Override
    public long executePartOne(List<String> input) {
        List<Integer> phase = doPhase(parseInput(input.get(0)));
        for (int i = 0; i < 99; i++) {
            phase = doPhase(phase);
        }
        AtomicInteger result = new AtomicInteger();
        AtomicInteger position = new AtomicInteger(10000000);
        phase.stream().limit(8).forEach(digit -> {
            result.addAndGet(digit * position.get());
            position.set(position.get() / 10);
        });
        return result.get();
    }

    @Override
    public long executePartTwo(List<String> input) {
        String phase = parseInputTimes10000(input.get(0));
        for (int i = 0; i < 100; i++) {
            phase = doPhase2(phase);
        }
        AtomicInteger result = new AtomicInteger();
        AtomicInteger position = new AtomicInteger(10000000);
        Arrays.stream(phase.split("(?!^)")).map(Integer::parseInt).skip(getOffset(input.get(0))).limit(8).forEach(digit -> {
            result.addAndGet(digit * position.get());
            position.set(position.get() / 10);
        });
        return result.get();
    }
}
