package de.michaelschmitz.days;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day10 implements Day {

    private Position vaporize(Position station, Map<Position, Integer> map) {
        return map.entrySet().stream().filter(item -> item.getValue() != -1)
                .sorted(Comparator.comparing(item -> computeAngle(station, item.getKey(), map))).toList().get(200).getKey();
    }

    private double computeAngle(Position station, Position astroid, Map<Position, Integer> map) {
        int x = astroid.x - station.x;
        int y = station.y - astroid.y;
        double angle = Math.atan2(x, y);
        if (angle < 0)
            angle = 2 * Math.PI + angle;
        return angle + 2 * Math.PI * countInWay(station, astroid, map);
    }

    private void countLineOfSight(Map<Position, Integer> map) {
        for (Position a : map.keySet()) {
            if (map.get(a) != -1) {
                int val = 0;
                for (Position b : map.keySet()) {
                    if (map.get(b) != -1 && a != b)
                        val += isObstructed(a, b, map) ? 0 : 1;
                }
                map.put(a, val);
            }
        }
    }

    private boolean isObstructed(Position a, Position b, Map<Position, Integer> map) {
        return countInWay(a, b, map) != 0;
    }

    private int countInWay(Position a, Position b, Map<Position, Integer> map) {
        Position d = new Position(b.x - a.x, b.y - a.y);
        int gcd = BigInteger.valueOf(d.x).gcd(BigInteger.valueOf(d.y)).intValue();
        if (gcd == 1) {
            return 0;
        } else {
            int count = 0;
            for (int i = 1; i < gcd; i++) {
                if (map.getOrDefault(new Position(a.x + (d.x / gcd * i), a.y + (d.y / gcd * i)), -1) >= 0) {
                    count++;
                }
            }
            return count;
        }
    }

    @Override
    public long executePartOne(List<String> input) {
        var map = calculateMapPartOne(input);
        return map.values().stream().mapToInt(v -> v).max().orElse(-1);
    }

    @Override
    public long executePartTwo(List<String> input) {
        var map = calculateMapPartOne(input);
        Position maxPos = map.entrySet().stream().max(Map.Entry.comparingByValue()).orElseThrow().getKey();
        return vaporize(maxPos, map).toInt();
    }

    private Map<Position, Integer> calculateMapPartOne(List<String> input) {
        Map<Position, Integer> map = new HashMap<>();

        AtomicInteger y = new AtomicInteger();
        input.forEach(input_val -> {
            for (int x = 0; x < input_val.length(); x++) {
                char c = input_val.charAt(x);
                map.put(new Position(x, y.get()), c == '#' ? 0 : -1);
            }
            y.getAndIncrement();
        });
        countLineOfSight(map);
        return map;
    }
    private static class Position {
        int x;
        int y;

        Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        int toInt() {
            return x * 100 + y;
        }

        @Override
        public String toString() {
            return x + "," + y;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Position) {
                return this.x == ((Position) o).x && this.y == ((Position) o).y;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return (x + "," + y).hashCode();
        }
    }
}
