package de.michaelschmitz.days;

import java.util.List;

public class Day01 implements Day {
    @Override
    public long executePartOne(List<String> input) {
        return input.stream().mapToInt(line -> Integer.parseInt(line) / 3 - 2).sum();
    }

    @Override
    public long executePartTwo(List<String> input) {
        return input.stream().mapToInt(line -> {
            int module = 0;
            int tmpSum = Integer.parseInt(line) / 3 - 2;
            while (tmpSum > 0) {
                module += tmpSum;
                tmpSum = tmpSum / 3 - 2;
            }
            return module;
        }).sum();
    }
}
