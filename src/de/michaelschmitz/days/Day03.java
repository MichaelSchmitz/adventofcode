package de.michaelschmitz.days;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Day03 implements Day {

    private static int findManhattanDist(Map<Pos, Integer> one, Map<Pos, Integer> two) {
        int min = Integer.MAX_VALUE;
        for (Pos p : one.keySet()) {
            if (p.toInt() < min && two.containsKey(p))
                min = p.toInt();
        }
        return min;
    }

    private static int findSteps(Map<Pos, Integer> one, Map<Pos, Integer> two) {
        AtomicInteger min = new AtomicInteger();
        min.set(Integer.MAX_VALUE);
        one.forEach((Pos key, Integer val) -> min.set(two.containsKey(key) && two.get(key) + val < min.get() ? two.get(key) + val : min.get()));
        return min.get();
    }

    private static Map<Pos, Integer> convertToList(String input) {
        Map<Pos, Integer> map = new HashMap<>();
        int curX = 0;
        int curY = 0;
        int steps = 0;
        String[] directions = input.split(",");
        for (String dir : directions) {
            switch (dir.substring(0, 1)) {
                case "L" -> {
                    int count = Integer.parseInt(dir.substring(1));
                    for (int i = 0; i < count; i++) {
                        map.putIfAbsent(new Pos(curX - i, curY), steps);
                        steps++;
                    }
                    curX -= count;
                }
                case "R" -> {
                    int count = Integer.parseInt(dir.substring(1));
                    for (int i = 0; i < count; i++) {
                        map.putIfAbsent(new Pos(curX + i, curY), steps);
                        steps++;
                    }
                    curX += count;
                }
                case "U" -> {
                    int count = Integer.parseInt(dir.substring(1));
                    for (int i = 0; i < count; i++) {
                        map.putIfAbsent(new Pos(curX, curY + i), steps);
                        steps++;
                    }
                    curY += count;
                }
                case "D" -> {
                    int count = Integer.parseInt(dir.substring(1));
                    for (int i = 0; i < count; i++) {
                        map.putIfAbsent(new Pos(curX, curY - i), steps);
                        steps++;
                    }
                    curY -= count;
                }
            }
        }
        map.remove(new Pos(0, 0));
        return map;
    }

    @Override
    public long executePartOne(List<String> input) {
        Map<Pos, Integer> one = convertToList(input.get(0));
        Map<Pos, Integer> two = convertToList(input.get(1));

        return findManhattanDist(one, two);
    }

    @Override
    public long executePartTwo(List<String> input) {
        Map<Pos, Integer> one = convertToList(input.get(0));
        Map<Pos, Integer> two = convertToList(input.get(1));

        return findSteps(one, two);
    }

    private static class Pos {
        int x;
        int y;

        Pos(int x, int y) {
            this.x = x;
            this.y = y;
        }

        int toInt() {
            return Math.abs(x) + Math.abs(y);
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Pos) {
                return this.x == ((Pos) o).x && this.y == ((Pos) o).y;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return (x + "," + y).hashCode();
        }
    }
}
