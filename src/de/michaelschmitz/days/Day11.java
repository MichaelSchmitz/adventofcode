package de.michaelschmitz.days;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day11 implements Day {

    private long runCode(Map<Position, Boolean> paint, long input, String code) {
        try {
            List<LinkedBlockingQueue<Long>> streams = Stream.generate((Supplier<LinkedBlockingQueue<Long>>) LinkedBlockingQueue::new)
                    .limit(2).toList();
            streams.get(0).put(input);
            RobotThread robot = new RobotThread(streams.get(1), streams.get(0), paint);
            BrainThread brain = new BrainThread(streams.get(0), streams.get(1), code);
            robot.start();
            brain.start();
            brain.join();
            robot.interrupt();
            return paint.size();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public long executePartOne(List<String> input) {
        Map<Position, Boolean> paint = new HashMap<>();
        return runCode(paint, 0L, input.get(0));
    }

    @Override
    public long executePartTwo(List<String> input) {
        Map<Position, Boolean> paint = new HashMap<>();
        paint.put(new Position(0, 0), true);
        runCode(paint, 1L, input.get(0));
        int maxX = paint.keySet().stream().max(Comparator.comparingInt(pos -> pos.x)).get().x;
        int maxY = paint.keySet().stream().max(Comparator.comparingInt(pos -> pos.y)).get().y;
        int minX = paint.keySet().stream().min(Comparator.comparingInt(pos -> pos.x)).get().x;
        int minY = paint.keySet().stream().min(Comparator.comparingInt(pos -> pos.y)).get().y;
        for (int y = maxY; y >= minY; y--) {
            for (int x = minX; x <= maxX; x++) {
                System.out.print(paint.getOrDefault(new Position(x, y), false) ? "X" : " ");
            }
            System.out.println();
        }
        return -1;
    }

    private static class Position {
        int x;
        int y;

        Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Position) {
                return this.x == ((Position) o).x && this.y == ((Position) o).y;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return (x + "," + y).hashCode();
        }
    }

    private static class RobotThread extends Thread {
        BlockingQueue<Long> in;
        BlockingQueue<Long> out;

        Map<Position, Boolean> paint;

        RobotThread(BlockingQueue<Long> in, BlockingQueue<Long> out, Map<Position, Boolean> paint) {
            this.in = in;
            this.out = out;
            this.paint = paint;
        }

        @Override
        public void run() {
            try {
                boolean firstOutput = true;
                int x = 0;
                int y = 0;
                int dir = 0;
                while (true) {
                    boolean one = in.take().equals(1L);
                    if (firstOutput) {
                        paint.put(new Position(x, y), one);
                    } else {
                        if (!one) // left
                            dir--;
                        else //right
                            dir++;
                        dir = (dir + 4) % 4;
                        switch (dir) {
                            case 0 -> y++;
                            case 1 -> x++;
                            case 2 -> y--;
                            case 3 -> x--;
                        }
                        out.put(paint.getOrDefault(new Position(x, y), false) ? 1L : 0L);
                    }
                    firstOutput = !firstOutput;
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }


    private static class BrainThread extends Thread {
        long relativeBase = 0;
        BlockingQueue<Long> in;
        BlockingQueue<Long> out;
        String input;

        BrainThread(BlockingQueue<Long> in, BlockingQueue<Long> out, String input) {
            this.in = in;
            this.out = out;
            this.input = input;
        }

        public void run() {
            try {
                doProcessing(input);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private Instruction getInstruction(long inst, Map<Long, Long> op, long ip, int size) {
            long val1 = getParam(op, ip, getMode(inst, 1), 1, inst % 10 == 3);
            long val2 = -1;
            long val3 = -1;
            if (size >= 2)
                val2 = getParam(op, ip, getMode(inst, 2), 2, inst % 10 == 3);
            if (size >= 3)
                val3 = getParam(op, ip, getMode(inst, 3), 3, true);
            return new Instruction(val1, val2, val3);
        }

        private long getParam(Map<Long, Long> op, long ip, int mode, int pos, boolean rel) {
            Long tmp = op.getOrDefault(ip + pos, 0L);
            switch (mode) {
                case 0:
                    return rel ? tmp : op.getOrDefault(tmp, 0L);
                case 1:
                    return tmp;
                case 2:
                    return rel ? relativeBase + tmp : op.getOrDefault(relativeBase + tmp, 0L);
                default:
                    System.err.println("Wrong mode");
                    return -1;
            }
        }

        private int getMode(long inst, int pos) {
            return (int) (inst / Math.pow(10, pos + 1) % 10);
        }

        long doProcessing(String input) throws InterruptedException {
            String[] opTmp = input.split(",");
            Map<Long, Long> op = new HashMap<>();
            for (long i = 0; i < opTmp.length; i++) {
                op.put(i, Long.parseLong(opTmp[(int) i]));
            }
            for (long i = 0; i < op.size(); ) {
                long currentCode = op.getOrDefault(i, 0L);
                switch ((int) (currentCode % 100)) {
                    case 1: {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 + inst.val2);
                        i += 4;
                        break;
                    }
                    case 2: {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 * inst.val2);
                        i += 4;
                        break;
                    }
                    case 3: {
                        Instruction inst = getInstruction(currentCode, op, i, 1);
                        op.put(inst.val1, in.take());
                        i += 2;
                        break;
                    }
                    case 4: {
                        Instruction inst = getInstruction(currentCode, op, i, 1);
                        out.put(inst.val1);
                        i += 2;
                        break;
                    }
                    case 5: {
                        Instruction inst = getInstruction(currentCode, op, i, 2);
                        if (inst.val1 != 0L) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                        break;
                    }
                    case 6: {
                        Instruction inst = getInstruction(currentCode, op, i, 2);
                        if (inst.val1 == 0L) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                        break;
                    }
                    case 7: {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 < inst.val2 ? 1L : 0L);
                        i += 4;
                        break;
                    }
                    case 8: {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 == inst.val2 ? 1L : 0L);
                        i += 4;
                        break;
                    }
                    case 9: {
                        relativeBase += getInstruction(currentCode, op, i, 1).val1;
                        i += 2;
                        break;
                    }
                    case 99: {
                        return op.getOrDefault(0L, 0L);
                    }
                    default: {
                        System.err.println("False opcode");
                        return -1L;
                    }
                }
            }
            return -1L;
        }

        private static class Instruction {
            long val1;
            long val2;
            long target;

            Instruction(long val1, long val2, long target) {
                this.val1 = val1;
                this.val2 = val2;
                this.target = target;
            }
        }
    }
}
