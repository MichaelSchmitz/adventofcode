package de.michaelschmitz.days;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day06 implements Day {
    private static final Map<String, Planet> planets = new HashMap<>();

    private static int getYouSan() {
        Map<String, Integer> key1 = new HashMap<>();
        key1.put("YOU", 0);
        String latest1 = "YOU";
        Map<String, Integer> key2 = new HashMap<>();
        key1.put("SAN", 0);
        String latest2 = "SAN";
        int count1 = 0;
        int count2 = 0;
        while (!key1.equals(key2)) {
            if (planets.get(latest1).orbit != null && key2.containsKey(planets.get(latest1).orbit.name))
                return count1 + 1 + key2.get(planets.get(latest1).orbit.name);
            if (planets.get(latest2).orbit != null && key1.containsKey(planets.get(latest2).orbit.name))
                return count2 + 1 + key1.get(planets.get(latest2).orbit.name);
            if (planets.get(latest2).orbit != null) {
                count2++;
                latest2 = planets.get(latest2).orbit.name;
                key2.put(latest2, count2);
            }
            if (planets.get(latest1).orbit != null) {
                count1++;
                latest1 = planets.get(latest1).orbit.name;
                key1.put(latest1, count1);
            }
        }
        return count1 + count2;
    }

    private static void addPlanet(String input) {
        String planet = input.substring(input.indexOf(")") + 1);
        String orbit = input.substring(0, input.indexOf(")"));
        if (!planets.containsKey(orbit)) {
            planets.put(orbit, new Planet(orbit));
        }
        if (planets.containsKey(planet)) {
            planets.get(planet).orbit = planets.get(orbit);
        } else {
            planets.put(planet, new Planet(planets.get(orbit), planet));
        }
    }

    @Override
    public long executePartOne(List<String> input) {
        input.forEach(Day06::addPlanet);
        return planets.values().stream().mapToInt(Planet::getCount).sum();
    }

    @Override
    public long executePartTwo(List<String> input) {
        input.forEach(Day06::addPlanet);
        return getYouSan() - 2;
    }

    private static class Planet {
        Planet orbit;
        String name;

        Planet(Planet orbit, String name) {
            this.orbit = orbit;
            this.name = name;
        }

        Planet(String name) {
            this.orbit = null;
            this.name = name;
        }

        int getCount() {
            if (orbit == null)
                return 0;
            else
                return orbit.getCount() + 1;
        }
    }

}
